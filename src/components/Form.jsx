import React from 'react'
import { useState, useEffect } from 'react'

const Form = () => {
    const [formValues, setFormValues] = useState({ username:"", email:"", phone:"", date:"", massage:""})
    const [formErrors, setFormErrors] = useState({})
    const [isSubmit, setIsSubmit] = useState(false)

    const handleChange = (e) => {
        const { name, value } = e.target
        
        setFormValues({...formValues, [name]:value})
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        setFormErrors(validateForm(formValues))
        setIsSubmit(true)
    }
    const validateForm = (formValues) => {
        const errors = {}
        const userRegExp = /^(([a-zA-z]{3,10})\s([a-zA-z]{3,10}))$/;
        const emailRegEx = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
        const phoneRegExp = /^((\+7|7|8)+([0-9]){10})$/;

        if(!formValues.username){
            errors.username = "Введите имя пользователя"
        }else if(!userRegExp.test(formValues.username)){
            errors.username = "Не верный формат!"
        }
        if(!formValues.email){
            errors.email = "Введите email"
        }else if(!emailRegEx.test(formValues.email)){
            errors.email = "Не верный формат!"
        }
        if(!formValues.phone){
            errors.phone = "Введите номер телефона"
        }else if(!phoneRegExp.test(formValues.phone)){
            errors.phone = "Не верный формат номера!"
        }
        if(!formValues.date){
            errors.date = "Заполните дату рождения"
        }
        if(!formValues.massage){
            errors.massage = "Введите сообщение"
        }else if(formValues.massage.length < 10){
            errors.massage = "Сообщни слишком короткое!" 
        }else if(formValues.massage.length > 300){
            errors.massage = "Сообщни сообщение должно содержать до 300 символов!" 
        }
        return errors
    }

    async function formSubmit(){
        
        const response = await sendForm(formValues);
        alert('PhP для отпраки?')
        //if (response.ok){

            //alert("Форма успешно отправлена")
            
        //} else {
           //alert("Error:" +  response.status);
        //}
    }

    async function sendForm (data){
        await fetch("",{
            method: "POST",
            body: data
        }) 
    }
    
    useEffect(()=>{
        if(Object.keys(formErrors).length === 0 && isSubmit){
            formSubmit()
        }
    },[formErrors])


    return(
        <form className="form" onSubmit={handleSubmit}>
            <div className="input-field">
                <label>Имя Фамилия</label> 
                <input className="input" type="text" placeholder='Имя Фамилия' name="username" value={formValues.username} onChange={handleChange}/> 
                <p className="error-massage">{formErrors.username}</p>     
            </div>
            <div className="input-field">
                <label>E-mail</label> 
                <input className="input" type="text" placeholder='emeil' name="email" value={formValues.email} onChange={handleChange}/> 
                <p className="error-massage">{formErrors.email}</p>     
            </div>
            <div className="input-field">
                <label>Телефон</label> 
                <input className="input" type="tel" placeholder='+7(___) 999-99-99' name="phone" value={formValues.phone} onChange={handleChange}/>  
                <p className="error-massage">{formErrors.phone}</p>    
            </div>
            <div className="input-field">
                <label>Дата рождения</label>
                <input className="input" type="date" name="date" value={formValues.date} onChange={handleChange}/>
                <p className="error-massage">{formErrors.date}</p>
            </div>
            <div className="input-field">
                <label>Сообщение</label>
                <textarea className='massage' name="massage" value={formValues.massage} onChange={handleChange}></textarea>
                <p className="error-massage">{formErrors.massage}</p>
            </div>
            <div className='button-wrapper'>
                <button className="button">Отправить</button>
            </div>  
        </form>
    )
}
export default Form